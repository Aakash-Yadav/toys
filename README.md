
# Cython Flask Api
![alt text](https://xp.io/storage/1QhEoFHM.png)

### Flask Url Shortener Generator

Using SQL Flask and Flask redirect i created a URL shorten service

### requirements Module

1. pip3 install Flask 
2. pip3 install flask_sqlalchemy
3. pip3 install cython

### Cython
Cython is a programming language that aims to be a superset of the Python programming language, designed to give C-like performance with code that is written mostly in Python with optional additional C-inspired syntax. Cython is a compiled language that is typically used to generate CPython extension modules

### Flask 

Flask is a micro web framework written in Python. It is classified as a microframework because it does not require particular tools or libraries. It has no database abstraction layer, form validation, or any other components where pre-existing third-party libraries provide common functions.

### validators
Python has all kinds of validation tools, but every one of them requires defining a schema. I wanted to create a simple validation library where validating a simple value does not require defining a form or a schema. 

### flask_sqlalchemy

Using raw SQL in Flask web applications to perform CRUD operations on database can be tedious. Instead, SQLAlchemy, a Python toolkit is a powerful OR Mapper that gives application developers the full power and flexibility of SQL. Flask-SQLAlchemy is the Flask extension that adds support for SQLAlchemy to your Flask application.


### What is ORM (Object Relation Mapping)?

Most programming language platforms are object oriented. Data in RDBMS servers on the other hand is stored as tables. Object relation mapping is a technique of mapping object parameters to the underlying RDBMS table structure. An ORM API provides methods to perform CRUD operations without having to write raw SQL statements.


## Check Out link = https://toy.pythonanywhere.com/Shortener

### Testing / Working 

_python3 code explain_
![alt text](https://xp.io/storage/1VYz5uOW.jpg)

*Json response*
![alt text](https://xp.io/storage/1VYG57xC.jpg)
