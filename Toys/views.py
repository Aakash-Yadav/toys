
# from dataclasses import dataclass
from flask import Blueprint, jsonify,request,redirect
from .db_function import add_to_db 
# from validators import url 
from .db_function import random_exist_or_not
views = Blueprint('views',__name__)

@views.post("/Shortener")
def short_url():
    user_given_data =  request.form.get("URL");
    data_to_send = add_to_db(user_given_data);
    return jsonify(data_to_send)

@views.get('/<n>')

def back_to_home(n):
    try:    return  redirect(random_exist_or_not(n).url)
    except: return jsonify({'Key':[n,False,None]})
