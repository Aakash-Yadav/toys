
from . import db


class Url_shortener(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    random = db.Column(db.String(8), unique=True, nullable=False)
    url = db.Column(db.Text, nullable=False)

    __slots__ = ("random_", 'url_')

    def __init__(self, random_, url_) -> None:

        self.random = random_ 
        self.url = url_ 
