from flask import Flask
from flask_sqlalchemy import SQLAlchemy 
 
from os import urandom,listdir

app = Flask(__name__)

db = SQLAlchemy()

'CONFIG APPLICATION'

app.config['SECRET_KEY'] = "%s"%(urandom(50))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///shortener.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = 0;

db.init_app(app=app);

from .views import views 

app.register_blueprint(views,url_prefix='/')

from .db_model import Url_shortener


def creat_data_base(app_):
    if "shortener.db" in listdir('.'):
        pass 
    else:
        db.create_all(app=app_)

creat_data_base(app);


