from . import db
from .db_model import Url_shortener
from CApi import generator 
from concurrent.futures import ProcessPoolExecutor

def process_pool_executor(fun,value):
    exe = ProcessPoolExecutor().submit(fun,value);
    return exe.result()


url_exist_or_not = lambda n : Url_shortener.query.filter_by(url=n).first()
random_exist_or_not = lambda n : Url_shortener.query.filter_by(random=n).first()
  

def Get_Key(url):
    
    Key = generator.return_key()
    Url_check = url_exist_or_not(url)
    
    if random_exist_or_not(Key):
        return Get_Key(url)
    elif Url_check:
        return  (Url_check.random,0)
    else:
        return (Key,1)

def add_to_db(url):
    
    add_value,key = process_pool_executor(Get_Key,url);
    
    if key==0 and add_value:
        return({'Given Url':url,'Ucode':add_value,'short':'toy.pythonanywhere.com/%s'%(add_value)})
    elif not add_value  and not key:
            return({'Url':url,'responce':None})
    else:
        db.session.add(Url_shortener(url_=url,random_=add_value))
        db.session.commit()
        return({'Given Url':url,'Ucode':add_value,'short':'toy.pythonanywhere.com/%s'%(add_value)}) 