
from cpython cimport array 
from random import randint


cdef array.array KEYS = array.array('i',[48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122])

cdef random_key():
    cdef short int x
    cdef str K = "";
    cdef array.array vlaues = array.array('i',[0 for x in range(8)])
    for x in range(8):
        vlaues[x] =  KEYS[randint(0,61)];
    K = "".join([chr(x) for x in vlaues])
    return K 
    
cpdef return_key():
    return random_key()


