from setuptools import setup
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("generator.pyx")
)

from os import listdir,system 


for i in listdir('.'):
    if i=='build' or i == 'generator.c':
        system('rm -rf %s'%(i))
    else:
        continue;
